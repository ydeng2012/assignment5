#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

# -----------
# imports
# -----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'},
         {'title': 'Algorithm Design', 'id': '2'},
         {'title': 'Python', 'id': '3'}]


@app.route('/book/JSON/')
def bookJSON():
    return jsonify(books)


@app.route('/')
@app.route('/book/')
def showBook():
    return render_template("showBook.html", books=books)


@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        newOne = {'title': request.form['newName']}
        idx = 1
        for book in books:
            if int(book['id']) == idx:
                idx += 1
            else:
                break
        newOne['id'] = str(idx)
        books.append(newOne)
        books.sort(key=lambda x: x['id'])
        return redirect(url_for('showBook'))
    return render_template('newBook.html', books=books)


@app.route('/book/<int:book_id>/edit/', methods=['GET', 'POST'])
def editBook(book_id):
    if request.method == 'POST':
        editName = request.form['editName']
        for book in books:
            if book['id'] == str(book_id):
                book['title'] = editName
        return redirect(url_for('showBook'))
    return render_template('editBook.html', book_id=book_id, books=books)


@app.route('/book/<int:book_id>/delete/', methods=['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        delBook = {}
        for book in books:
            if book['id'] == str(book_id):
                delBook = book
                break
        books.remove(delBook)
        return redirect(url_for('showBook'))
    return render_template('deleteBook.html', book_id=book_id, books=books)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
